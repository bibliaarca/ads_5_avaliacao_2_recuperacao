package appdispositivosmoveis.edu.ifsc.br.apprecuperacao;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewDuasColunasSimples extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcula);


        //Associando a variavel local listView a o objeto instaciado no layout com id listView
        ListView listView = findViewById(R.id.listView);

        //Carregando Dados predefinidos chamando o controlador Frutas e solicitando a lista de frutas
        Simulacao coluna= new Simulacao();

        //Adiquirindo lista de frutas em formato String que serão apresentados no listView
        ArrayList<HashMap<String,String>> listaDados = new ArrayList<>();
        HashMap<String,String> item = new HashMap<String, String>();
        for (ColunaSimulacao f: coluna.DADOS) {
            item = new HashMap<String,String>();
            item.put("1", Double.toString(f.getAmortizacao()));
            item.put("2", Integer.toString(f.getParcela()));
            listaDados.add(item);
        }

        /**Carregando um adapter com a estrutura das informações que configuram o ListView,
         * O Adapter tem a responsabidade de instanciar o layout definido para cada linha, para o número de dados passado no arrayList,
         * além disso ele o adapter atribui cada registro de dados os campos informados que estão no layout.
         *
         * Diferente do ArrayAdapater que associa uma string para cada linha do ListViews,
         * o simplesAdapater possibilita que passemos um ArrayList com itens de dados,e cada item sendo um HashMap
         * O que possibilita definirmos varios dados que serão associados para cada linha do ListView a ser montado,
         *
         * Paramêtros
         * 1º parametro - Contexto da classe atual
         * 2º parametro - ArrayList com HashMap  de strings que contém os dados a serem atribuidos a cada linha do listView
         * 3º parametro - Array de String para nomear cada os dados inseridos
         * 4º parametro - Array de inteiro com os id dos Campos definidos no layout que devem ser preenchidos com dados.
         * */
        SimpleAdapter simpleAdapter = new SimpleAdapter(getApplicationContext(),
                listaDados,
                android.R.layout.two_line_list_item,
                new String[] { "line1","line2" },
                new int []  {android.R.id.text1,android.R.id.text2} );


        //Solicita ao listView que carregue o layout e dados passados no arrayAdapter e apresente
        listView.setAdapter(simpleAdapter);
    }



}
