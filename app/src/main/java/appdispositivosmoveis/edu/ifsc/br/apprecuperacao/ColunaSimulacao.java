package appdispositivosmoveis.edu.ifsc.br.apprecuperacao;


public class ColunaSimulacao {

    private int parcela;
    private float amortizacao;
    private float saldodevedor;
    private float valor_prime;
    private float valor_sac;

    public ColunaSimulacao(int parcela, float amortizacao, float saldodevedor, float valor_prime, float valor_sac) {
        this.parcela = parcela;
        this.amortizacao = amortizacao;
        this.saldodevedor = saldodevedor;
        this.valor_prime  = valor_prime;
        this.valor_sac = valor_sac;
    }

    public int getParcela() {
        return parcela;
    }

    public void setParcela(int parcela) {
        this.parcela = parcela;
    }

    public float getAmortizacao() {
        return amortizacao;
    }

    public void setAmortizacao(float amortizacao) {
        this.amortizacao = amortizacao;
    }

    public float getSaldodevedor() {
        return saldodevedor;
    }

    public void setSaldodevedor(float saldodevedor) {
        this.saldodevedor = saldodevedor;
    }

    public float getValor_prime() {
        return valor_prime;
    }

    public void setValor_prime(float valor_prime) {
        this.valor_prime = valor_prime;
    }

    public float getValor_sac() {
        return valor_sac;
    }


}
